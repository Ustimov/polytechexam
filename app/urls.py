from django.conf.urls import url, include
from app.views import *


urlpatterns = [
    url(r'^auth/$', auth, name='auth'),
    url(r'^products/$', product_list, name='product_list'),
    url(r'^orders/(?P<amount>[0-9]+)/$', order_list, name='order_list'),
    url(r'^product/(?P<id>[0-9]+)/$', product_detail, name='product'),
    url(r'^order/$', post_order, name='post_order'),
    url(r'^logout/$', logoff, name='logout'),
]