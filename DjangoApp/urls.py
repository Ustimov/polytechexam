from django.conf.urls import url, include
from app.views import *
from app.models import *
from django.contrib.auth.views import *
from django.contrib import admin
from django.conf.urls.static import static


urlpatterns = [
    # Examples:
    url(r'^$', home, name='home'),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('app.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
